package wibble.mods.auroraGsi;

import com.google.gson.*;
import net.minecraft.client.settings.KeyBinding;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.lang.reflect.Type;

/**
 * Task that will send a single JSONified request to the Aurora HTTP server.
 */
public class SendGameState implements Runnable {

    private Gson gson; // Create a Gson that will be used to encode the object into a JSON string.
    private GSINode rootNode = new GSINode();

    public SendGameState() {
        // Add a custom serializer to the JSON-builder so that only relevant KeyBinding fields are returned.
        GsonBuilder builder = new GsonBuilder();
        JsonSerializer<KeyBinding> serializer = new JsonSerializer<KeyBinding>() {
            @Override
            public JsonElement serialize(KeyBinding src, Type typeOfSrc, JsonSerializationContext context) {
                JsonObject jObject = new JsonObject();
                jObject.addProperty("keyCode", src.getKeyCode());
                jObject.addProperty("modifier", "NONE"); // This is required by Aurora (since it is present in newer MC versions)
                jObject.addProperty("context", "UNIVERSAL"); // ^^
                return jObject;
            }
        };
        builder.registerTypeAdapter(KeyBinding.class, serializer);

        gson = builder.create();
    }

    public void run() {
        try {
            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost request = new HttpPost("http://localhost:" + AuroraGSI.AuroraPort);
            request.addHeader("Content-Type", "application/json");
            request.setEntity(new StringEntity(gson.toJson(rootNode.update()))); // Update and stringify the GameStateNode and use it to set the request's body.
            httpClient.execute(request); // Execute the request, but don't worry about the response from the server.

        } catch (Exception ignore) { }
    }
}
